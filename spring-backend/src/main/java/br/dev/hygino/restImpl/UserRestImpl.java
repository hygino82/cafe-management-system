package br.dev.hygino.restImpl;

import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import br.dev.hygino.constants.CafeConstants;
import br.dev.hygino.rest.UserRest;
import br.dev.hygino.services.UserService;
import br.dev.hygino.utils.CafeUtils;

@RestController
public class UserRestImpl implements UserRest {

	final UserService userService;

	public UserRestImpl(UserService userService) {
		this.userService = userService;
	}

	@Override
	public ResponseEntity<String> signUp(Map<String, String> requestMap) {
		try {
			this.userService.signUp(requestMap);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return CafeUtils.getResponseEntity(CafeConstants.SOMETHING_WENT_WRONG, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
